#University of Hawaii, College of Engineering
#brief   Lab03b - cat 'N Mouse 2 - EE 205 - Spr 2022
#
# This Shell Script  program will:
#   1.  Play the Cat and mouse game from lab 2 but in bash
#   Just do ./catnmouse.sh
#
# @file    catnmouse2.sh
# @author  @Trevor Chang <@Trevorkw@hawaii.edu>
# @date    @1 Feb 2022

#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo "Ok cat, I'm thinking of a number from 1 to $THE_MAX_VALUE.  Make a guess..."
read n
while [ $n != $THE_NUMBER_IM_THINKING_OF ];
do
   if [ $n -gt $THE_MAX_VALUE ]; then
      echo "You must enter a number that is <= $THE_MAX_VALUE"
      echo "Ok cat, I'm thinking of a number from 1 to $THE_MAX_VALUE.  Make a guess..."
      read n
   fi
   if [ $n -lt 1 ]; then
      echo "You must enter a number that is >= 1"
      echo "Ok cat, I'm thinking of a number from 1 to $THE_MAX_VALUE.  Make a guess..."
      read n
   fi
   if [ $n -gt $THE_NUMBER_IM_THINKING_OF ]; then
      echo "No cat... the number I'm thinking of is smaller than $n"
      echo "Ok cat, I'm thinking of a number from 1 to $THE_MAX_VALUE.  Make a guess..."
      read n
   fi
   if [ $n -lt $THE_NUMBER_IM_THINKING_OF ]; then
      echo "No cat... the number I'm thinking of is larger than $n"
      echo "Ok cat, I'm thinking of a number from 1 to $THE_MAX_VALUE.  Make a guess..."
      read n
   fi
done
while [ $n == $THE_NUMBER_IM_THINKING_OF ]
do
   echo "You got me."
   echo "|\---/|"
   echo "| o_o |"
   echo " \_^_/"
   break
done
